module.exports = {
	type:"postgres",
	migrations:['./src/dbOrm/migrations/*.*'],
	port:process.env.POSTGRES_PORT,
	host:process.env.POSTGRES_HOST,
	database:process.env.API_DATABASE_NAME,
	username:process.env.POSTGRES_USER,
	synchronize:false,
	migrationsTableName: "nestjs_migrations",
	cli: {
		migrationsDir: "./src/dbOrm/migrations"
	}
}