import {ObjectType,Field,Int} from "type-graphql";

@ObjectType()
export class Students{

	@Field(type=>Int)
	id:number;

	@Field()
	name:string;

	@Field()
	dob:Date;
}