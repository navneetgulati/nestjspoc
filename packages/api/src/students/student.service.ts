import { Injectable } from "@nestjs/common";
import { Connection, Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Students } from "./student.entity";

@Injectable()
export class StudentService{
	constructor(@InjectRepository(Students) private readonly studentRepo:Repository<Students>){}

	async findStudentById(id:number){
	const students=await this.studentRepo.findByIds([id]);
	return students[0];
	}

	async fetchAllStudents(){
		return await this.studentRepo.query('SELECT * FROM students');
	}

	async insertStudent(name:string,dob:Date){
		const response= await this.studentRepo.insert({name,dob});
		console.log(response.identifiers);
		return true;
	}
}