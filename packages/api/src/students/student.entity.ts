import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Students{

	@PrimaryGeneratedColumn()
	id:number;

	@Column()
	name:string;

	@Column()
	dob:Date;
}