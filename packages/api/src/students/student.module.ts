import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Students } from "./student.entity";
import { StudentService } from "./student.service";
import { StudentResolver } from "./student.resolver";

@Module({
	imports:[TypeOrmModule.forFeature([Students])],
	providers:[StudentService,StudentResolver]
})
export class StudentModule{}