import { Resolver, Query, Args, Mutation } from "@nestjs/graphql";
import { Students } from "./student.model";
import { Int } from "type-graphql";
import { StudentService } from "./student.service";

@Resolver(of=>Students)
export class StudentResolver{

	constructor(private readonly studentService:StudentService){}

	@Query(returns=>Students,{name:'student'})
	async getStudent(@Args({name:'id',type:()=>Int}) id:number){
		return await this.studentService.findStudentById(id);
	}

	@Query(returns=>[Students],{name:'students'})
	async getStudents(){
		return await this.studentService.fetchAllStudents();
	}

	@Mutation()
	async insertStudent(@Args({name:'name'}) name:string,@Args('dob') dob:Date){
		return await this.studentService.insertStudent(name,dob);
	}
}