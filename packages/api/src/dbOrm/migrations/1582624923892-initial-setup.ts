import {MigrationInterface, QueryRunner} from "typeorm";

export class initialSetup1582624923892 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
			await queryRunner.query(`
			CREATE TABLE students(
				id SERIAL PRIMARY KEY, 
				name TEXT,
				dob DATE
				)`);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
			await queryRunner.query(`
			DROP TABLE students
			`);
    }

}
