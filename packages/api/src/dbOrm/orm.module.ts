import {Module} from "@nestjs/common";
import {TypeOrmModule, TypeOrmModuleOptions} from "@nestjs/typeorm";
import {NestConfigModule} from "../config/config.module";
import { NestConfigService } from "../config/config.service";
import { PostgresConnectionOptions } from "typeorm/driver/postgres/PostgresConnectionOptions";

@Module({
	imports:[
		TypeOrmModule.forRootAsync({
			imports:[NestConfigModule],
			inject:[NestConfigService],
			useFactory:async (configService:NestConfigService)=>({
					type:'postgres' as 'postgres',
				...configService.databaseConfig,
				autoLoadEntities:true
			} as TypeOrmModuleOptions),
		}),
	]
})

export class DBOrmModule {}