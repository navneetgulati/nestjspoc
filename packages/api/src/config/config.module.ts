import {Module} from "@nestjs/common";
import {ConfigModule, ConfigService} from "@nestjs/config";
import config from "./configuration";
import * as Joi from "@hapi/joi";
import { NestConfigService } from "./config.service";

@Module({
	imports:[
		ConfigModule.forRoot({
			load:[config],
			isGlobal:true,
			validationSchema:Joi.object({
				database: Joi.object({
					port:Joi.string(),
					host:Joi.string(),
					name:Joi.string(),
					user:Joi.string()
				})
			})
	})],
	providers:[ConfigService,NestConfigService],
	exports:[ConfigService,NestConfigService]
})
export class NestConfigModule {};