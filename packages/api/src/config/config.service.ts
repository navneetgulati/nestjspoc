import {Injectable} from "@nestjs/common";
import {ConfigService} from "@nestjs/config";
import { TypeOrmModuleOptions } from "@nestjs/typeorm";

@Injectable()
export class NestConfigService{

	constructor(private configService:ConfigService){}

	get databaseConfig():TypeOrmModuleOptions{
		return this.configService.get('nestjsConfig.database');
	}

}