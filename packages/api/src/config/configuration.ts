import {registerAs} from "@nestjs/config";


export default registerAs('nestjsConfig',()=>({
	database:{
		migrations:['../dbOrm/migrations/*.ts'],
		port:process.env.POSTGRES_PORT,
		host:process.env.POSTGRES_HOST,
		database:process.env.API_DATABASE_NAME,
		username:process.env.POSTGRES_USER,
		synchronize:false,
		migrationsTableName: "nestjs_migrations",
		cli: {
			migrationsDir: "../dbOrm/migrations"
	}
	}
}));