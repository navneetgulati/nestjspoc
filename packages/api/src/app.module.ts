import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {DBOrmModule} from "./dbOrm/orm.module";
import {NestConfigModule} from "./config/config.module";
import { GraphqlModule } from './utils/graphql.module';
import { StudentModule } from './students/student.module';

@Module({
  imports: [DBOrmModule,NestConfigModule,StudentModule,GraphqlModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
