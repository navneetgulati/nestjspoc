import {Module} from "@nestjs/common";
import {GraphQLModule} from "@nestjs/graphql";
import {join} from "path";

@Module({
	imports:[GraphQLModule.forRoot({
		autoSchemaFile:true
	})]
})
export class GraphqlModule{}